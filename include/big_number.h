#ifndef BIG_NUMBER_H
#define BIG_NUMBER_H

#include <stdint.h>

enum            sign
{
  POSITIVE = 0,
  NEGATIVE = 1,
};

typedef struct
{
  uint8_t*  number;
  uint32_t  size;
  char      sign;
} big_number;

typedef struct
{
  big_number* q;
  big_number* r;
  int         zero_divide;
} div_res;

void        init_big_numbers();
void        free_big_numbers();
big_number* parse_binary(char* str);
big_number* parse_decimal(char* str);
int         get_byte(big_number *n, int c);
int         get_bit(big_number* n, int c);
big_number* add_values(big_number* n1, big_number* n2);
int         number_cmp(big_number* n1, big_number* n2);
big_number* add_numbers(big_number* n1, big_number* n2);
big_number* sub_numbers(big_number* n1, big_number* n2);
big_number* mult_2(big_number* n, int autofree);
big_number* div_2(big_number* n, int autofree);
big_number* copy_number(big_number* n);
big_number* mult_numbers(big_number* n1, big_number* n2);
div_res     div_values(big_number* x, big_number* y);
div_res     div_numbers(big_number* x, big_number* y);
void        change_sign(big_number* n);
void        free_number(big_number* n);
void        print_debug(big_number* n);
void        print_binary(big_number* n);
void        print_decimal(big_number* n);

#endif
