
RM			= rm -f

NAME		= bistro

CC			= gcc

CFLAGS	=  -I include -Wall -Werror

SRC 		= src/main.c				\
					src/big_number.c	\

OBJ			= $(SRC:.c=.o)

$(NAME):$(OBJ)
	$(CC) $(OBJ) -o $(NAME)

all:	$(NAME)

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
