#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "big_number.h"

static big_number* ZERO = 0;
static big_number* ONE = 0;
static big_number* TEN = 0;

void init_big_numbers()
{
  ZERO = parse_binary("0");
  ONE = parse_binary("1");
  TEN = parse_binary("1010");
}

void free_big_numbers()
{
  free_number(ZERO);
  free_number(ONE);
  free_number(TEN);
}

// parse a positive binary number
// remember to use free_number
big_number*   parse_binary(char* str)
{
  int         len;
  big_number* n;

  len = strlen(str);
  n = malloc(sizeof(big_number));
  n->sign = POSITIVE;
  n->size = 1 + len / 8;
  n->number = malloc(sizeof(uint8_t) * n->size);
  memset(n->number, 0, n->size);

  for (int i=0; i < len; i++)
  {
    if (str[len-1-i] == '1')
    {
      int byte_loc;
      int bit_loc;

      byte_loc = i / 8;
      bit_loc = i % 8;
      n->number[byte_loc] |= 1 << bit_loc;
    }
  }
  return n;
}

big_number* parse_decimal(char* str)
{
  if (strlen(str) == 0)
    return copy_number(ZERO);
  return 0; // TODO
}

// return the value of a number's byte or 0 if out of bounds
int         get_byte(big_number* n, int c)
{
  if (n->size <= c)
    return 0;
  else
    return n->number[c];
}

// return the value of a number's bit or 0 if out of bounds
int         get_bit(big_number* n, int c)
{
  return (get_byte(n, c/8) & (1 << c%8)) > 0;
}

// add values of 2 numbers, return a positive number
// remember to use free_number
big_number* add_values(big_number* n1, big_number* n2)
{
  big_number* res;
  int         ret;

  res = malloc(sizeof(big_number));
  res->sign = POSITIVE;
  if (n1->size > n2->size)
    res->size = n1->size + 1;
  else
    res->size = n2->size + 1;
  res->number = malloc(sizeof(uint8_t) * res->size);
  memset(res->number, 0, res->size);

  ret = 0;
  for (int i=0; i<res->size; i++)
  {
    int res_byte = ret + get_byte(n1, i) + get_byte(n2, i);
    ret = res_byte > 255;
    res->number[i] = res_byte;
  }
  return res;
}

// substract 2 numbers, always returns a positive number
// remember to use free_number
big_number* sub_values(big_number* n1, big_number* n2)
{
  big_number* res;
  big_number* tmp;
  int         ret;

  if (number_cmp(n1, n2) == -1)
  {
    tmp = n1;
    n1 = n2;
    n2 = tmp;
  }

  res = malloc(sizeof(big_number));
  res->sign = POSITIVE;
  if (n1->size > n2->size)
    res->size = n1->size;
  else
    res->size = n2->size;
  res->number = malloc(sizeof(uint8_t) * res->size);
  memset(res->number, 0, res->size);

  ret = 0;
  for (int i=0; i<res->size; i++)
  {
    int res_byte = ret + get_byte(n1, i) - get_byte(n2, i);
    ret = -(res_byte < 0);
    res->number[i] = res_byte;
  }
  return res;
}

// compare to numbers
// return 1 if the first is bigger
// return 0 if they are equal
// return -1 otherwise
// ignore signs
int         number_cmp(big_number* n1, big_number* n2)
{
  int size;

  if (n1->size > n2->size)
    size = n1->size;
  else
    size = n2->size;
  for (int i=size-1; i>=0;i--)
  {
    if (get_byte(n1, i) > get_byte(n2, i))
      return 1;
    else if (get_byte(n1, i) < get_byte(n2, i))
      return -1;
  }
  return 0;
}

// add two numbers taking signs into account
// remember to use free_number
big_number* add_numbers(big_number* n1, big_number* n2)
{
  big_number* res;

  if (n1->sign == n2->sign)
  {
    res = add_values(n1, n2);
    res->sign = n1->sign;
    return res;
  }
  else
  {
    char sign;
    if (number_cmp(n1, n2) == 1)
      sign = n1->sign;
    else
      sign = n2->sign;
    res = sub_values(n1, n2);
    res->sign = sign;
    return res;
  }
}

// substract two numbers taking signs into account
// remember to use free_number
big_number* sub_numbers(big_number* n1, big_number* n2)
{
  big_number* res;

  if (n1->sign == n2->sign)
  {
    char sign;
    if (number_cmp(n1, n2) == 1)
      sign = n1->sign;
    else
      sign = 1-n2->sign;
    res = sub_values(n1, n2);
    res->sign = sign;
    return res;
  }
  else
  {
    res = add_values(n1, n2);
    res->sign = n1->sign;
    return res;
  }
}

// double a number
big_number* mult_2(big_number* n, int autofree)
{
  big_number* res;

  res = malloc(sizeof(big_number));
  res->sign = n->sign;
  res->size = n->size;
  if (n->number[n->size-1] > (1<<7))
    res->size += 1;
  res->number = malloc(sizeof(uint8_t) * res->size);
  memset(res->number, 0, res->size);

  for (int i=1; i<res->size*8; i++)
  {
    if (get_bit(n, i-1))
      res->number[i/8] += 1<<(i%8);
  }
  if (autofree)
    free_number(n);
  return res;
}

// divide a number by 2
big_number* div_2(big_number* n, int autofree)
{
  big_number* res;

  res = malloc(sizeof(big_number));
  res->sign = n->sign;
  res->size = n->size;
  if (n->number[n->size-1] <= 1)
    res->size -= 1;
  res->number = malloc(sizeof(uint8_t) * res->size);
  memset(res->number, 0, res->size);

  for (int i=0; i<res->size*8; i++)
  {
    if (get_bit(n, i+1))
    res->number[i/8] += 1<<(i%8);
  }
  if (autofree)
    free_number(n);
  return res;
}

big_number* copy_number(big_number* n)
{
  big_number* res;

  res = malloc(sizeof(big_number));
  res->sign = n->sign;
  res->size = n->size;
  res->number = malloc(sizeof(uint8_t) * n->size);
  for (int i=0; i<n->size; i++)
    res->number[i] = n->number[i];
  return res;
}

big_number* mult_numbers(big_number* n1, big_number* n2)
{
  big_number* res;
  big_number* n2_pow;
  big_number* tmp1;

  res = malloc(sizeof(big_number));
  res->size = 0;
  res->sign = n1->sign != n2->sign;
  res->number = malloc(0);
  n2_pow = copy_number(n2);

  for (int bit=0; bit<n1->size*8; bit++)
  {
    if (get_bit(n1, bit))
    {
      tmp1 = res;
      res = add_numbers(res, n2_pow);
      free_number(tmp1);
    }
    n2_pow = mult_2(n2_pow, 1);
  }
  free_number(n2_pow);
  return res;
}

div_res       div_values(big_number* x, big_number* y)
{
  div_res     res;

  if (number_cmp(y, ZERO) == 0)
  {
    res.q = 0;
    res.r = 0;
    res.zero_divide = 1;
    return res;
  }
  else
    res.zero_divide = 0;

  if (number_cmp(x, y) <= 0)
  {
    res.q = copy_number(ZERO);
    res.r = copy_number(x);
  }
  else
  {
    big_number* Y = mult_2(y, 0);
    div_res res2 = div_values(x, Y);
    res.q = mult_2(res2.q, 1);
    if (number_cmp(res2.r, y) >= 0)
        {
          big_number* tmp = res.q;
          res.q = add_values(ONE, res.q);
          res.r = sub_values(res2.r, y);
          free_number(res2.r);
          free_number(tmp);
        }
    else
        res.r = res2.r;
    free_number(Y);
  }
  return res;
}

div_res     div_numbers(big_number* x, big_number* y)
{
  div_res   res;

  res = div_values(x, y);
  if (!res.zero_divide)
  {
    if (x->sign == y->sign)
      res.q->sign = POSITIVE;
    else
      res.q->sign = NEGATIVE;
    res.r->sign = x->sign;
  }
  return res;
}

void        change_sign(big_number* n)
{
  n->sign = 1-n->sign;
}

void        free_number(big_number* n)
{
  free(n->number);
  free(n);
}

void        print_debug(big_number* n)
{
  printf("DEBUG\t:\n");
  printf("SIZE\t: %i\n", n->size);
  printf("SIGN\t: %c\n", n->sign);
  for (int i=0; i<n->size; i++)
    printf("%i_", n->number[i]);
  printf("\n");
}

void        print_binary(big_number* n)
{
  char ignore_zero;

  if (n->sign == NEGATIVE)
  {
    printf("-");
  }
  ignore_zero = 1;
  for (int i=n->size-1; i>=0; i--)
    for (int j=7; j>=0; j--)
    {
      if (n->number[i] & (1 << j))
      {
        printf("1");
        ignore_zero = 0;
      }
      else if (!ignore_zero)
      {
        printf("0");
      }
    }
  if (ignore_zero)
    printf("0");
  printf("\n");
}

void      print_decimal_rec(big_number* n, int first_call)
{
  if (number_cmp(n, ZERO) == 0)
    {
      if (first_call)
        printf("0");
      else if (n->sign == NEGATIVE)
        printf("-");
      return;
    }
  div_res div = div_numbers(n, TEN);
  print_decimal_rec(div.q, 0);
  printf("%c", div.r->number[0] + '0');
  free_number(div.q);
  free_number(div.r);
}

void        print_decimal(big_number* n)
{
  print_decimal_rec(n, 1);
  printf("\n");
}
