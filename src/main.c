#include "big_number.h"

int main(int ac, char **av)
{
  init_big_numbers();

  big_number* n2;
  big_number* n3;
  big_number* n4;
  big_number* n5;
  big_number* n6;
  big_number* n7;
  big_number* n8;
  div_res     r;

  n2 = parse_binary("111111111111111");
  print_decimal(n2);

  n3 = parse_binary("01001000100001000001000000");
  print_decimal(n3);

  n4 = add_numbers(n2, n3);
  print_decimal(n4);

  n5 = sub_numbers(n3, n2);
  print_decimal(n5);

  n6 = sub_numbers(n2, n3);
  print_decimal(n6);

  n7 = mult_2(n6, 0);
  print_decimal(n7);

  n8 = mult_numbers(n2, n3);
  print_decimal(n8);
  n8 = div_2(n8, 1);
  print_decimal(n8);
  r = div_values(n3, n2);
  print_decimal(r.q);
  print_decimal(r.r);

  free_number(n2);
  free_number(n3);
  free_number(n4);
  free_number(n5);
  free_number(n6);
  free_number(n7);
  free_number(n8);
  free_number(r.q);
  free_number(r.r);

  free_big_numbers();

  return 0;
}
